This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### Requirements

* [NodeJS](https://nodejs.org/en/)

### How to run

1. ```npm start```

### How to test

1. ```npm test```
