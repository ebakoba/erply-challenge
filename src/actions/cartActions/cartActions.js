import actionTypes from '../../constants/actionTypes';

const actions = {
  addProductToCart: (id, quantity) => ({
    type: actionTypes.ADD_PRODUCT_TO_CART,
    id,
    quantity,
  }),

  deleteProductFromCart: id => ({
    type: actionTypes.DELETE_PRODUCT_FROM_CART,
    id,
  }),
};

export default actions;
