/* global describe, it, expect */
import actionTypes from '../../constants/actionTypes';
import actions from './index';

describe('cartActions', () => {
  it('returns action for adding products to cart', () => {
    const id = 2;
    const quantity = 4;
    const expectedAction = {
      type: actionTypes.ADD_PRODUCT_TO_CART,
      id,
      quantity,
    };

    expect(actions.addProductToCart(id, quantity)).toEqual(expectedAction);
  });

  it('returns action for deleting products from cart', () => {
    const id = 3;
    const expectedAction = {
      type: actionTypes.DELETE_PRODUCT_FROM_CART,
      id,
    };

    expect(actions.deleteProductFromCart(id)).toEqual(expectedAction);
  });
});
