
import sillyNames from 'sillyname';
import randomColor from 'randomcolor';
import actionTypes from '../../constants/actionTypes';

const randomCode = ceil => Math.floor(Math.random() * ceil);

const actions = {
  fetchProducts: () => async (dispatch) => {
    /*
      const response = await fetch('/list', {
        method: 'GET',
        headers: {
          AUTH: 'fae7b9f6-6363-45a1-a9c9-3def2dae206d',
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        const products = await response.json();
        dispatch(actions.productsFetchSuccess(products));
        dispatch(actions.changeProductsLoadedStatus(true));
      }
    */
    const replacementProducts = [];

    for (let i = 1; i < 500; i += 1) {
      replacementProducts.push({
        currency: 'EUR',
        department: sillyNames(),
        description: 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
        id: i,
        image: `http://dummyimage.com/231x205.jpg/${randomColor({ luminosity: 'dark' }).split('#')[1]}/ffffff`,
        instock: Math.random() < 0.5,
        name: sillyNames(),
        price: randomCode(2000),
        productcode: `${randomCode(99999)}-${randomCode(999)}`,
        store: Math.random() < 0.5 ? 'Finland' : 'Estonia',
      });
    }

    dispatch(actions.productsFetchSuccess([
      ...replacementProducts,
    ]));
    dispatch(actions.changeProductsLoadedStatus(true));
  },

  changeProductsLoadedStatus: status => ({
    type: actionTypes.PRODUCTS_LOADED_STATUS_CHANGED,
    status,
  }),

  productsFetchSuccess: products => ({
    type: actionTypes.PRODUCTS_FETCH_SUCCESS,
    products,
  }),

  updateProductFilter: filter => ({
    type: actionTypes.UPDATE_PRODUCT_FILTER,
    filter,
  }),
};

export default actions;
