/* global describe, it, expect */
import fetchMock from 'fetch-mock';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import actionTypes from '../../constants/actionTypes';
import actions from './index';

const mockStore = configureStore([thunk]);

describe('productActions', () => {
  const products = [
    {
      currency: 'EUR',
      department: 'Healthy Food',
      description: 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
      id: 101,
      image: 'http://dummyimage.com/231x205.jpg/ff4444/ffffff',
      instock: false,
      name: 'Chinese Foods - Cantonese',
      price: 86,
      productcode: '61442-173',
      store: 'Finland',
    },
  ];

  it('calls productFetchSuccess with right parameters after successful fetch', async () => {
    fetchMock.getOnce('/list', [
      ...products,
    ]);

    const store = mockStore({ products: {} });

    await store.dispatch(actions.fetchProducts());
    expect(store.getActions()[0]).toEqual(actions.productsFetchSuccess([...products]));
  });

  it('returns action containing products', () => {
    const expectedAction = {
      type: actionTypes.PRODUCTS_FETCH_SUCCESS,
      products: [...products],
    };

    expect(actions.productsFetchSuccess([...products])).toEqual(expectedAction);
  });

  it('returns action for loaded status change', () => {
    const expectedAction = {
      type: actionTypes.PRODUCTS_LOADED_STATUS_CHANGED,
      status: true,
    };

    expect(actions.changeProductsLoadedStatus(true)).toEqual(expectedAction);
  });

  it('returns action for products filter update', () => {
    const filter = {
      countriesList: [],
      instock: false,
      outstock: true,
    };

    const expectedAction = {
      type: actionTypes.UPDATE_PRODUCT_FILTER,
      filter,
    };

    expect(actions.updateProductFilter(filter)).toEqual(expectedAction);
  });
});
