import React from 'react';
import PropTypes from 'prop-types';
import currencyFormatter from 'currency-formatter';

export const ProductPrice = ({ price, instock, currency }) => (
  <div className="price">
    <b>{currencyFormatter.format(
      price,
      { code: currency },
    )}
    </b>
    {instock ? (
      <span className="badge badge-success">In stock</span>
    ) : (
      <span className="badge badge-danger">Out of stock</span>
    )}
  </div>
);

ProductPrice.propTypes = {
  price: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  instock: PropTypes.bool.isRequired,
};

export default ProductPrice;
