/* global expect, it, describe */

import React from 'react';
import { shallow } from 'enzyme';
import { ProductPrice } from './ProductPrice';

describe('ProductPrice', () => {
  const props = {
    price: 20,
    currency: 'USD',
    instock: false,
  };

  it('renders successfully', () => {
    const component = shallow(<ProductPrice {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders successfully with different currency', () => {
    const component = shallow(<ProductPrice {...{ ...props, currency: 'EUR' }} />);
    expect(component).toMatchSnapshot();
  });

  it('renders successfully with instock true', () => {
    const component = shallow(<ProductPrice {...{ ...props, instock: true }} />);
    expect(component).toMatchSnapshot();
  });
});
