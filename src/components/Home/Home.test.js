/* global expect, it, describe */

import React from 'react';
import { shallow } from 'enzyme';
import { Home } from './Home';

describe('Home', () => {
  const products = [
    {
      currency: 'EUR',
      department: 'Healthy Food',
      description: 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
      id: 101,
      image: 'http://dummyimage.com/231x205.jpg/ff4444/ffffff',
      instock: false,
      name: 'Chinese Foods - Cantonese',
      price: 86,
      productcode: '61442-173',
      store: 'Finland',
    },
  ];


  it('renders successfully', () => {
    const component = shallow(<Home products={products} currentPage={1} />);
    expect(component).toMatchSnapshot();
  });
});
