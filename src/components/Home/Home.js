import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HomeProduct from '../HomeProduct';
import Paginator from '../Paginator';
import './Home.css';

export const Home = ({ products, currentPage }) => {
  const maxPage = Math.ceil(products.length / 100);
  const pageToShow = Math.min(currentPage, maxPage);
  const productsList = products.slice(100 * (pageToShow - 1), 100 * pageToShow)
    .map(product =>
      (
        <HomeProduct
          product={product}
          key={product.id}
        />
      ));

  return (
    <React.Fragment>
      <Paginator currentPage={pageToShow} maxPage={maxPage} />
      <div className="card-columns">
        {productsList}
      </div>
      <Paginator currentPage={pageToShow} maxPage={maxPage} />
    </React.Fragment>
  );
};

Home.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  currentPage: PropTypes.number.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  const { match: { params } } = ownProps;
  const filter = state.productsFilter;
  const products = Object.values(state.products)
    .filter(product => filter.countriesFilter.indexOf(product.store) < 0)
    .filter(product => (product.instock === filter.instock || product.instock === !filter.outstock)
      && (filter.instock || filter.outstock));
  return {
    products,
    currentPage: parseInt(params.id, 10) || 1,
  };
};

export default connect(mapStateToProps)(Home);
