/* global expect, it, describe, jest */

import React from 'react';
import { shallow } from 'enzyme';
import { Product } from './Product';

describe('Product', () => {
  const product = {
    currency: 'EUR',
    department: 'Healthy Food',
    description: 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
    id: 101,
    image: 'http://dummyimage.com/231x205.jpg/ff4444/ffffff',
    instock: false,
    name: 'Chinese Foods - Cantonese',
    price: 86,
    productcode: '61442-173',
    store: 'Finland',
    quantity: 4,
  };

  const props = {
    product,
    deleteProductFromCart: jest.fn(),
    isInCart: true,
  };

  it('renders successfully', () => {
    const component = shallow(<Product {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders component without delete button when isInCart prop is false', () => {
    const component = shallow(<Product {...{ ...props, isInCart: false }} />);
    expect(component).toMatchSnapshot();
  });

  it('calls deleteProductFromCart prop when delete button is clicked', () => {
    const component = shallow(<Product {...props} />);
    const deleteButton = component.find('.btn-danger');
    deleteButton.simulate('click');
    expect(props.deleteProductFromCart).toHaveBeenCalledWith(product.id);
  });
});
