import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import cartActions from '../../actions/cartActions';
import ProductPrice from '../ProductPrice';
import ProductCart from '../ProductCart';
import './Product.css';

export const Product = ({ product, deleteProductFromCart, isInCart }) => (

  <div className="jumbotron jumbotron-fluid">
    <div className="media">
      <div className="row">
        <div className="col-sm-3 image-holder">
          <img className="img-fluid" src={product.image} alt="Generic placeholder" />
        </div>
        <div className="col-sm-9">
          <div className="media-body">
            <div className="container">
              <h1 className="display-4">{product.name}</h1>
              <p className="lead">{product.description}</p>
              <hr />
              <p><b>Department: </b>{product.department}</p>
              <p><b>Store: </b>{product.store}</p>
              <div className="lead price">
                <ProductPrice
                  price={product.price}
                  currency={product.currency}
                  instock={product.instock}
                />
                <ProductCart id={product.id} instock={product.instock} />
              </div>
              { isInCart ? (
                <div className="remove-holder">
                  <button
                    className="btn btn-danger"
                    onClick={() => deleteProductFromCart(product.id)}
                  >
                    Remove from cart
                  </button>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    instock: PropTypes.bool.isRequired,
  }).isRequired,
  deleteProductFromCart: PropTypes.func.isRequired,
  isInCart: PropTypes.bool.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  const { match: { params } } = ownProps;
  const isInCart = state.cart[params.id] !== undefined;
  return {
    product: state.products[params.id],
    isInCart,
  };
};

const mapDispatchToProps = dispatch => ({
  deleteProductFromCart: id => dispatch(cartActions.deleteProductFromCart(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
