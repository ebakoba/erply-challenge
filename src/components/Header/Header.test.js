/* global expect, it, describe */

import React from 'react';
import { shallow } from 'enzyme';
import { Header } from './Header';

describe('Header', () => {
  it('renders successfully', () => {
    const component = shallow(<Header currentPath="/" numberOfProductsInCart={4} />);
    expect(component.exists()).toEqual(true);
  });

  it('sets correct classname to active path', () => {
    const cartActiveComponent = shallow(<Header currentPath="/cart" numberOfProductsInCart={4} />);

    expect(cartActiveComponent.find('[name="cart"]').hasClass('nav-item active')).toEqual(true);
    expect(cartActiveComponent.find('[name="home"]').parent().hasClass('nav-item active')).toEqual(false);
  });
});
