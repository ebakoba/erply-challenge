import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

export const Header = (props) => {
  const isActive = path => classnames('nav-item', {
    active: props.currentPath === path,
  });
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars" aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon" />
      </button>

      <div className="collapse navbar-collapse" id="navbars">
        <ul className="navbar-nav mr-auto">
          <li name="home" className={isActive('/')}>
            <Link to="/" className="nav-link">Store</Link>
          </li>
        </ul>
        <ul className="navbar-nav ml-auto">
          <li name="cart" className={isActive('/cart')}>
            <Link to="/cart" className="nav-link">
              <span>
                {props.numberOfProductsInCart} <i className="fa fa-shopping-cart" />
              </span>
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

Header.propTypes = {
  currentPath: PropTypes.string.isRequired,
  numberOfProductsInCart: PropTypes.number.isRequired,
};

const mapStateToProps = (state) => {
  const numberOfProductsInCart = Object.keys(state.cart).length;
  return {
    numberOfProductsInCart,
    currentPath: state.router.location.pathname,
  };
};

export default connect(mapStateToProps)(Header);
