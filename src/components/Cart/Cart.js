import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import currencyFormatter from 'currency-formatter';
import cartActions from '../../actions/cartActions';
import './Cart.css';

export const Cart = ({ products, deleteProductFromCart, changeProductQuantity }) => {
  const productsList = products.map((product) => {
    const total = currencyFormatter
      .format(product.price * product.quantity, { code: product.currency });

    const changeQuantity = (event) => {
      changeProductQuantity(product.id, event.target.value - product.quantity);
    };

    return (
      <tr key={product.id}>
        <td>{product.name}</td>
        <td>
          <div className="input-group">
            <input
              className="quantity"
              type="number"
              min={1}
              defaultValue={product.quantity}
              onChange={event => changeQuantity(event)}
            />
          </div>
        </td>
        <td>{product.price}</td>
        <td>{total}</td>
        <td>
          <button
            className="btn btn-sm btn-danger"
            onClick={() => deleteProductFromCart(product.id)}
          >
            <i className="fa fa-trash" />
          </button>
        </td>
      </tr>
    );
  });

  const wholeTotal = currencyFormatter
    .format(products.map(product => product.price * product.quantity)
      .reduce((accumulation, oneTotal) => accumulation + oneTotal, 0), { code: 'EUR' });
  return (
    <div className="cart table-responsive">
      <table className="table table-sm table-hover">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
            <th scope="col">Total</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {productsList}
          <tr>
            <td />
            <td />
            <td />
            <td><b>{wholeTotal}</b></td>
            <td />
          </tr>
        </tbody>
      </table>
    </div>
  );
};

Cart.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
  }).isRequired),
  deleteProductFromCart: PropTypes.func.isRequired,
  changeProductQuantity: PropTypes.func.isRequired,
};

Cart.defaultProps = {
  products: [],
};

const mapStateToProps = (state) => {
  const productsInCart = Object.entries(state.cart).map(([id, quantity]) => ({
    ...state.products[id],
    quantity,
  }));

  return {
    products: productsInCart,
  };
};

const mapDispatchToProps = dispatch => ({
  deleteProductFromCart: id => dispatch(cartActions.deleteProductFromCart(id)),
  changeProductQuantity: (id, quantity) => dispatch(cartActions.addProductToCart(id, quantity)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
