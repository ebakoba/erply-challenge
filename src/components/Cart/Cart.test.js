/* global expect, it, describe, jest */

import React from 'react';
import { shallow } from 'enzyme';
import { Cart } from './Cart';

describe('Cart', () => {
  const products = [
    {
      currency: 'EUR',
      department: 'Healthy Food',
      description: 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
      id: 101,
      image: 'http://dummyimage.com/231x205.jpg/ff4444/ffffff',
      instock: false,
      name: 'Chinese Foods - Cantonese',
      price: 86,
      productcode: '61442-173',
      store: 'Finland',
      quantity: 4,
    },
  ];

  const props = {
    products,
    deleteProductFromCart: jest.fn(),
    changeProductQuantity: jest.fn(),
  };

  it('renders successfully', () => {
    const component = shallow(<Cart {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('calls delete prop on delete button click', () => {
    const component = shallow(<Cart {...props} />);
    const deleteButton = component.find('.btn-danger');
    deleteButton.simulate('click');
    expect(props.deleteProductFromCart).toHaveBeenCalledWith(products[0].id);
  });

  it('calls change product quantity prop on quantity change', () => {
    const component = shallow(<Cart {...props} />);
    const quantityInput = component.find('.quantity');
    quantityInput.simulate('change', { target: { value: 5 } });
    expect(props.changeProductQuantity).toHaveBeenCalledWith(101, 1);
  });
});
