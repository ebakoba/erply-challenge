/* global expect, it, describe, jest */

import React from 'react';
import { shallow, mount } from 'enzyme';
import { ProductCart } from './ProductCart';

describe('ProductCart', () => {
  const props = {
    addToCart: jest.fn(),
    id: 10,
    instock: true,
  };

  it('renders successfully', () => {
    const component = shallow(<ProductCart {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('disables add to cart button when instock prop is false', () => {
    const component = mount(<ProductCart {...{ ...props, instock: false }} />);
    const deleteButton = component.find('.btn-outline-secondary');
    deleteButton.simulate('click');
    expect(props.addToCart).not.toHaveBeenCalled();
  });

  it('calls addToCart prop when add button is clicked', () => {
    const component = shallow(<ProductCart {...props} />);
    const deleteButton = component.find('.btn-outline-secondary');
    deleteButton.simulate('click');
    expect(props.addToCart).toHaveBeenCalledWith(props.id, 1);
  });
});
