import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import cartActions from '../../actions/cartActions';

export class ProductCart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      quantity: 1,
      formControllClasses: classnames(
        'form-control',
        {
          'form-control-sm': this.props.smallButtons,
        },
      ),
      cartButtonClasses: classnames(
        'btn',
        'btn-outline-secondary',
        {
          'btn-sm': this.props.smallButtons,
        },
      ),
    };

    this.handleQuantityChange = this.handleQuantityChange.bind(this);
  }

  handleQuantityChange(event) {
    this.setState({
      quantity: parseInt(event.target.value, 10),
    });
  }

  render() {
    return (
      <div className="input-group">
        <input
          type="number"
          className={this.state.formControllClasses}
          min={1}
          step={1}
          value={this.state.quantity}
          onChange={this.handleQuantityChange}
        />
        <div className="input-group-append">
          <button
            className={this.state.cartButtonClasses}
            type="button"
            data-toggle="tooltip"
            data-placement="top"
            title="Add to cart"
            disabled={!this.props.instock}
            onClick={() => this.props.addToCart(this.props.id, this.state.quantity)}
          >
            <i className="fa fa-shopping-cart" />
          </button>
        </div>
      </div>
    );
  }
}

ProductCart.propTypes = {
  addToCart: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  instock: PropTypes.bool.isRequired,
  smallButtons: PropTypes.bool,
};

ProductCart.defaultProps = {
  smallButtons: false,
};

const mapDispatchToProps = dispatch => ({
  addToCart: (id, quantity) => dispatch(cartActions.addProductToCart(id, quantity)),
});

export default connect(null, mapDispatchToProps)(ProductCart);
