import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import productActions from '../../actions/productActions';
import defaultProductsFilter from '../../constants/defaultProductsFilter';
import './Filter.css';

export class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ...props.filter,
    };
  }

  isFilterInDefaultMode() {
    return JSON.stringify(this.state) === JSON.stringify(defaultProductsFilter);
  }

  handleCountryChange(event) {
    const countriesFilter = [
      ...this.state.countriesFilter,
    ];
    if (!event.target.checked) {
      countriesFilter.push(event.target.name);
    } else {
      countriesFilter.splice(countriesFilter.indexOf(event.target.name), 1);
    }
    this.setState({
      countriesFilter,
    });
  }


  handleStockChange(event) {
    this.setState({
      [event.target.name]: event.target.checked,
    });
  }

  restoreDefaults() {
    this.setState({
      ...defaultProductsFilter,
    }, () => {
      this.props.updateFilter({ ...this.state });
    });
  }


  render() {
    const countriesCheck = this.props.countries.map((country) => {
      const isChecked = !(this.state.countriesFilter.indexOf(country) > -1);

      return (
        <div className="form-check" key={country}>
          <label className="form-check-label" htmlFor={country}>
            <input
              checked={isChecked}
              type="checkbox"
              className="form-check-input"
              id={country}
              name={country}
              onChange={event => this.handleCountryChange(event)}
            />
            {country}
          </label>
        </div>
      );
    });

    const filterClassNames = classnames('btn', 'dropdown-toggle', {
      'btn-secondary': !this.isFilterInDefaultMode(),
      'btn-outline-secondary': this.isFilterInDefaultMode(),
    });

    return (
      <div
        className="dropdown filter"
        data-toggle="tooltip"
        data-placement="top"
        title="Filter"
      >
        <button className={filterClassNames} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i className="fa fa-filter" />
        </button>
        <div className="dropdown-menu dropdown-menu-right filter__drop" aria-labelledby="dropdownMenuButton">
          <form className="px-4 py-3">
            <div className="form-group">
              <h6>
                <b>
                  Store
                </b>
              </h6>
              {countriesCheck}
            </div>
            <div className="form-group">
              <h6>
                <b>
                  Availability
                </b>
              </h6>
              <div className="form-check">
                <label className="form-check-label" htmlFor="instock">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="instock"
                    name="instock"
                    checked={this.state.instock}
                    onChange={event => this.handleStockChange(event)}
                  />
                  In stock
                </label>
              </div>
              <div className="form-check">
                <label className="form-check-label" htmlFor="outstock">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="outstock"
                    name="outstock"
                    checked={this.state.outstock}
                    onChange={event => this.handleStockChange(event)}
                  />
                  Out of stock
                </label>
              </div>
            </div>
            <button
              type="button"
              className="btn btn-danger float-left"
              onClick={() => this.restoreDefaults()}
            >
              Remove
            </button>
            <button
              type="button"
              className="btn btn-primary float-right"
              onClick={() => this.props.updateFilter({ ...this.state })}
            >
              Apply
            </button>
          </form>
        </div>
      </div>
    );
  }
}

Filter.propTypes = {
  countries: PropTypes.arrayOf(PropTypes.string).isRequired,
  filter: PropTypes.shape({}).isRequired,
  updateFilter: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const countries = [...new Set(Object.values(state.products).map(product => product.store))];
  return {
    countries,
    filter: state.productsFilter,
  };
};

const mapDispatchToProps = dispatch => ({
  updateFilter: filter => dispatch(productActions.updateProductFilter(filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
