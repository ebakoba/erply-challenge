/* global expect, it, describe, jest */

import React from 'react';
import { shallow } from 'enzyme';
import defaultProductsFilter from '../../constants/defaultProductsFilter';
import { Filter } from './Filter';

describe('Filter', () => {
  it('renders successfully', () => {
    const props = {
      countries: ['Estonia', 'Finland'],
      filter: { ...defaultProductsFilter },
      updateFilter: jest.fn(),
    };
    const component = shallow(<Filter {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders successfully without default filter', () => {
    const props = {
      countries: ['Estonia', 'Finland'],
      filter: { ...defaultProductsFilter },
      updateFilter: jest.fn(),
    };
    const component = shallow(<Filter {...{ ...props, outstock: false }} />);
    expect(component).toMatchSnapshot();
  });

  it('calls updateFilter prop with defaultFilter on restore click', () => {
    const props = {
      countries: ['Estonia', 'Finland'],
      filter: { ...defaultProductsFilter },
      updateFilter: jest.fn(),
    };
    const component = shallow(<Filter {...props} />);
    const restoreButton = component.find('.btn-danger');
    restoreButton.simulate('click');
    expect(props.updateFilter).toHaveBeenCalledWith(defaultProductsFilter);
  });

  it('calls updateFilter prop with updated filter on apply click', () => {
    const props = {
      countries: ['Estonia', 'Finland'],
      filter: { ...defaultProductsFilter },
      updateFilter: jest.fn(),
    };
    const component = shallow(<Filter {...props} />);
    const instockCheckbox = component.find('#instock');
    instockCheckbox.simulate('change', { target: { checked: false, name: 'instock' } });
    const applyButton = component.find('.btn-primary');
    applyButton.simulate('click');
    expect(props.updateFilter).toHaveBeenCalledWith({ ...defaultProductsFilter, instock: false });
  });
});
