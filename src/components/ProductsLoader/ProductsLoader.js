import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import productActions from '../../actions/productActions';
import './ProductsLoader.css';

export class ProductsLoader extends React.Component {
  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    if (this.props.loaded) {
      return (
        <React.Fragment>
          {this.props.children}
        </React.Fragment>
      );
    }
    return (
      <div className="loader-holder">
        <div className="loader" />
      </div>
    );
  }
}

ProductsLoader.propTypes = {
  fetchProducts: PropTypes.func.isRequired,
  loaded: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

const mapDispatchToProps = dispatch => ({
  fetchProducts: () => dispatch(productActions.fetchProducts()),
});

const mapStateToProps = state => ({
  loaded: state.productsLoaded,
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductsLoader);
