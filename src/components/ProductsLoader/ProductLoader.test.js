/* global expect, it, describe, jest */

import React from 'react';
import { shallow } from 'enzyme';
import { ProductsLoader } from './ProductsLoader';

describe('ProductsLoader', () => {
  const props = {
    fetchProducts: jest.fn(),
    loaded: false,
  };

  it('renders successfully', () => {
    const component = shallow(<ProductsLoader {...props}><h2>Test</h2></ProductsLoader>);
    expect(component).toMatchSnapshot();
  });

  it('calls fetch product on mount', () => {
    shallow(<ProductsLoader {...props}><h2>Test</h2></ProductsLoader>);
    expect(props.fetchProducts).toHaveBeenCalled();
  });

  it('renders children on loaded true', () => {
    const component = shallow(<ProductsLoader {...props}><h2>Test</h2></ProductsLoader>);
    expect(component).toMatchSnapshot();
  });
});
