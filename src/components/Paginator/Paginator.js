import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import Filter from '../Filter';
import './Paginator.css';

export const Paginator = ({ currentPage, maxPage }) => {
  const pageLinks = [];
  for (let page = currentPage - 1; page < currentPage + 2 && page <= maxPage; page += 1) {
    const linkClassNames = classNames('page-item', {
      active: currentPage === page,
    });
    if (page > 0) {
      pageLinks.push(<li key={page} className={linkClassNames}><Link className="page-link" to={`/${page}`}>{page}</Link></li>);
    }
  }

  return (
    <div className="paginator-holder">
      <nav aria-label="Page navigation">
        <ul className="pagination">
          <li className="page-item">
            <Link className="page-link" to="/1" aria-label="First">
              <span aria-hidden="true">&laquo;</span>
              <span className="sr-only">First</span>
            </Link>
          </li>
          {pageLinks}
          <li className="page-item">
            <Link className="page-link" to={`/${maxPage}`} aria-label="Last">
              <span aria-hidden="true">&raquo;</span>
              <span className="sr-only">Last</span>
            </Link>
          </li>
        </ul>
      </nav>
      <Filter />
    </div>
  );
};

Paginator.propTypes = {
  currentPage: PropTypes.number.isRequired,
  maxPage: PropTypes.number.isRequired,
};

export default Paginator;
