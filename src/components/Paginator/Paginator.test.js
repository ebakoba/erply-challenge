/* global expect, it, describe */

import React from 'react';
import { shallow } from 'enzyme';
import { Paginator } from './Paginator';

describe('Paginator', () => {
  const props = {
    currentPage: 1,
    maxPage: 3,
  };

  it('renders successfully', () => {
    const component = shallow(<Paginator {...props} />);
    expect(component).toMatchSnapshot();
  });
});
