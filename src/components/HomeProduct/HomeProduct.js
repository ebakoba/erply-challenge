import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import ProductPrice from '../ProductPrice';
import ProductCart from '../ProductCart';

export const HomeProduct = ({ product, goToProduct }) => {
  const width = product.image.split('/')[3].split('x')[0];
  return (
    <div className="card">
      <div className="card-header">
        <h6 className="card-title"><b>{product.name}</b></h6>
      </div>
      <div className="product-image">
        <img className="card-img-top" style={{ width: `${width}px` }} src={product.image} alt={product.name} />
      </div>
      <div className="card-body">
        <p className="card-text">{product.description}</p>
        <div className="card-text price">
          <ProductPrice
            price={product.price}
            currency={product.currency}
            instock={product.instock}
          />
        </div>
        <div className="price lower-buttons">
          <button className="btn btn-sm btn-info" onClick={() => goToProduct(product.id)}>Details</button>
          <ProductCart smallButtons id={product.id} instock={product.instock} />
        </div>
      </div>
    </div>
  );
};

HomeProduct.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    instock: PropTypes.bool.isRequired,
  }).isRequired,
  goToProduct: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  goToProduct: id => dispatch(push(`/products/${id}`)),
});

export default connect(null, mapDispatchToProps)(HomeProduct);
