/* global expect, it, describe, jest */

import React from 'react';
import { shallow } from 'enzyme';
import { HomeProduct } from './HomeProduct';

describe('HomeProduct', () => {
  const product = {
    currency: 'EUR',
    department: 'Healthy Food',
    description: 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
    id: 101,
    image: 'http://dummyimage.com/231x205.jpg/ff4444/ffffff',
    instock: false,
    name: 'Chinese Foods - Cantonese',
    price: 86,
    productcode: '61442-173',
    store: 'Finland',
    quantity: 4,
  };

  const props = {
    product,
    goToProduct: jest.fn(),
  };

  it('renders successfully', () => {
    const component = shallow(<HomeProduct {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('calls delete prop on delete button click', () => {
    const component = shallow(<HomeProduct {...props} />);
    const deleteButton = component.find('.btn-info');
    deleteButton.simulate('click');
    expect(props.goToProduct).toHaveBeenCalledWith(product.id);
  });
});
