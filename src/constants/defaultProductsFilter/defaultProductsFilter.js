
const defaultProductsFilter = {
  countriesFilter: [],
  instock: true,
  outstock: true,
};

export default defaultProductsFilter;
