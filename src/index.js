/* global document */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { store, persistor, history } from './store';
import './index.css';
import ProductsLoader from './components/ProductsLoader';
import Header from './components/Header';
import Home from './components/Home';
import Product from './components/Product';
import Cart from './components/Cart';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <React.Fragment>
          <Header />
          <div className="container-fluid">
            <Switch>
              <ProductsLoader>
                <Switch>
                  <Route path="/products/:id" component={Product} />
                  <Route path="/cart" component={Cart} />
                  <Route path="/:id" component={Home} />
                  <Route path="/" component={Home} />
                </Switch>
              </ProductsLoader>
            </Switch>
          </div>
        </React.Fragment>
      </ConnectedRouter>
    </PersistGate>
  </Provider>
  ,
  document.getElementById('root'),
);
registerServiceWorker();
