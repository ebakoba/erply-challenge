/* global describe, it, expect */
import cartActions from '../../actions/cartActions';
import cart from '.';

describe('cartReducers', () => {
  const id = 1;
  const quantity = 3;

  it('returns the initial state of cart', () => {
    expect(cart(undefined, {})).toEqual({});
  });

  it('handles ADD_PRODUCT_TO_CART with new product', () => {
    const expectedCart = {
      [id]: quantity,
    };

    expect(cart(undefined, cartActions.addProductToCart(id, quantity))).toEqual(expectedCart);
  });

  it('handles ADD_PRODUCT_TO_CART with excisting product', () => {
    const cartState = {
      [id]: quantity,
    };

    const expectedCart = {
      [id]: quantity * 2,
    };

    expect(cart(cartState, cartActions.addProductToCart(id, quantity))).toEqual(expectedCart);
  });

  it('handles DELETE_PRODUCT_FROM_CART', () => {
    const cartState = {
      [id]: quantity,
    };

    expect(cart(cartState, cartActions.deleteProductFromCart(id))).toEqual({});
  });
});
