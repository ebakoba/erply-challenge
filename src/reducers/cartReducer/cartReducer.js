import { omit } from 'lodash';
import actionTypes from '../../constants/actionTypes';


const cart = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.ADD_PRODUCT_TO_CART: {
      const newState = { ...state };

      if (newState[action.id] === undefined) {
        newState[action.id] = action.quantity;
      } else {
        newState[action.id] += action.quantity;
      }
      return newState;
    }
    case actionTypes.DELETE_PRODUCT_FROM_CART:
      return {
        ...omit(state, [action.id]),
      };
    default:
      return state;
  }
};

export default cart;
