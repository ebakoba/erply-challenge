/* global describe, it, expect */
import productActions from '../../actions/productActions';
import defaultProductsFilter from '../../constants/defaultProductsFilter';
import { products as productsReducer, productsLoaded, productsFilter } from './index';

describe('productsReducers', () => {
  const products = [
    {
      currency: 'EUR',
      department: 'Healthy Food',
      description: 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
      id: 101,
      image: 'http://dummyimage.com/231x205.jpg/ff4444/ffffff',
      instock: false,
      name: 'Chinese Foods - Cantonese',
      price: 86,
      productcode: '61442-173',
      store: 'Finland',
    },
  ];

  it('returns the initial state of products', () => {
    expect(productsReducer(undefined, {})).toEqual({});
  });

  it('handles PRODUCTS_FETCH_SUCCESS', () => {
    expect(productsReducer(undefined, productActions.productsFetchSuccess(products))).toEqual({
      [products[0].id]: products[0],
    });
  });

  it('returns the intial state of productsLoaded', () => {
    expect(productsLoaded(undefined, {})).toEqual(false);
  });

  it('handles PRODUCTS_LOADED_STATUS_CHANGED', () => {
    expect(productsLoaded(undefined, productActions
      .changeProductsLoadedStatus(true))).toEqual(true);
  });

  it('returns the initial state of productsFilter', () => {
    expect(productsFilter(undefined, {})).toEqual(defaultProductsFilter);
  });

  it('handles UPDATE_PRODUCT_FILTER', () => {
    const newFilter = {
      countriesList: ['Finland'],
      instock: true,
      outstock: false,
    };

    expect(productsFilter(undefined, productActions
      .updateProductFilter(newFilter))).toEqual(newFilter);
  });
});
