import actionTypes from '../../constants/actionTypes';
import defaultProductsFilter from '../../constants/defaultProductsFilter';

export const products = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.PRODUCTS_FETCH_SUCCESS: {
      const newState = {};
      action.products.forEach((product) => {
        newState[product.id] = product;
      });
      return newState;
    }
    default:
      return state;
  }
};

export const productsLoaded = (state = false, action) => {
  switch (action.type) {
    case actionTypes.PRODUCTS_LOADED_STATUS_CHANGED:
      return action.status;
    default:
      return state;
  }
};

export const productsFilter = (state = defaultProductsFilter, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_PRODUCT_FILTER:
      return action.filter;
    default:
      return state;
  }
};

