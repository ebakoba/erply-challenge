import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import cart from './cartReducer';
import { products, productsLoaded, productsFilter } from './productReducers';

const rootReducers = combineReducers({
  cart,
  products,
  productsLoaded,
  productsFilter,
  router,
});

export default rootReducers;
